include(FetchContent)
#FetchContent_Declare(
#  famfam
#  URL http://www.famfamfam.com/lab/icons/silk/famfamfam_silk_icons_v013.zip
#  URL_HASH MD5=7c60fef364ca681448d39726930535de
#  SOURCE_DIR ${CMAKE_SOURCE_DIR}/resources/icons/silk)
FetchContent_Declare(
  googletest
  GIT_REPOSITORY https://github.com/google/googletest.git
  GIT_TAG 58d77fa8070e8cec2dc1ed015d66b454c8d78850 # release-1.12.1
)
#FetchContent_Declare(
#  qcustomplot
#  URL https://www.qcustomplot.com/release/2.1.0fixed/QCustomPlot-source.tar.gz
#  URL_HASH MD5=ecb2d59b440a52c38c5c3425eb7afa05)
#FetchContent_Declare(
#  qslog
#  GIT_REPOSITORY https://gitlab.com/syntgroup/libs/QsLog.git
#  GIT_TAG e45f5bc0449aeb573abe49712de09b16e9f643e2)
#FetchContent_Declare(
#  quazip
#  URL https://github.com/stachenov/quazip/archive/refs/tags/v1.3.tar.gz
#  URL_HASH SHA256=c1239559cd6860cab80a0fd81f4204e606f9324f702dab6166b0960676ee1754
#)
#FetchContent_MakeAvailable(famfam googletest qcustomplot qslog quazip)
FetchContent_MakeAvailable(googletest)


