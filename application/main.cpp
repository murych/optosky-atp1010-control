
#include <QCoreApplication>
#include <QDebug>

auto
main(int argc, char* argv[]) -> int
{

  QCoreApplication app{ argc, argv };

  qDebug() << "hello world";

  return QCoreApplication::exec();
}
