#include <gtest/gtest.h>

#include <lib.h>
#include <memory>

TEST(BackendTests, LibraryName)
{
  auto lib{ std::make_unique<library>() };
  EXPECT_EQ(lib->name, "optosky-ap1010-control");
}
